#ifndef DBX_CUT_H
#define DBX_CUT_H

#include <iostream>


struct AnaObjects {
     int           mlep;
     int           nlep;
};


class dbxCut : public TObject  {

public:
        dbxCut( string name){p_name=name;}
        dbxCut( string name, int i){p_name=name; p_cuti=i;}
       ~dbxCut(){}
          void setRa( float mn, float mx)   {p_min=mn; p_max=mx;}
          void setLi( float f) {p_cutf=f;}
          void setOp( string o){p_oper=o; }
        string getName(){return p_name;}
        string getOp(){return p_oper;}
           int getCi(){return p_cuti;} // a cut index
         float getLi(){return p_cutf;}
  virtual bool select(AnaObjects *ao){cout << "Mother here\n"; return 0;}
          bool Ccompare(float v){
               if (p_oper=="EQ") { return (v==p_cutf);}
               if (p_oper=="NE") { return (v!=p_cutf);}
               if (p_oper=="LE") { return (v<=p_cutf);}
               if (p_oper=="LT") { return (v< p_cutf);}
               if (p_oper=="GE") { return (v>=p_cutf);}
               if (p_oper=="GT") { return (v> p_cutf);}
               if (p_oper=="[]") { return ((v>=p_min) && (v<=p_max));}
               if (p_oper=="]]") { return ((v> p_min) && (v<=p_max));}
               if (p_oper=="][") { return ((v> p_min) && (v< p_max));}
               if (p_oper=="[[") { return ((v>=p_min) && (v< p_max));}
         };
private:
       string p_name;
       string p_oper;
          int p_cuti;
        float p_cutf, p_min, p_max;

       ClassDef(dbxCut,1);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ NLEP
class dbxCutNLep : public dbxCut {
 public:
      dbxCutNLep( ): dbxCut("NLEP"){}
      dbxCutNLep( int i): dbxCut("NLEP", i){}
      bool select(AnaObjects *ao){ return Ccompare(ao->nlep); } 
private:
       ClassDef(dbxCutNLep,1);
};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MLEP
class dbxCutMLep : public dbxCut {
 public:
      dbxCutMLep( ): dbxCut("MLEP"){}
      dbxCutMLep( int i): dbxCut("MLEP", i){}
      bool select(AnaObjects *ao){ return Ccompare(ao->mlep); } 
private:
       ClassDef(dbxCutMLep,1);
};


#endif

objects := $(patsubst %.cpp,%.o,$(wildcard *.cpp))

ROOTCFLAGS    = $(shell root-config --cflags)
CXXFLAGS     += $(ROOTCFLAGS) -I../analysis_core  -D__STANDALONE__
CXX           = $(shell root-config --cxx) -g


%.o : %.cxx
	$(CXX) $(CXXFLAGS) -c $< -o $@

test : $(objects)
	root  -q -b -x makeSos.C
clean:
	rm -f *.o ; rm -f *.so ; rm -rf *.d
build : $(objects) 
	g++ basic_parser.cpp -o basic_parser.exe

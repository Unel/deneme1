# this is the exploratory prototyping for cutlang/LHADA
## this is not a fully functional analysis tool, just explores some possibilities

this package provides the following
 * a stand alone code for evaluating a cutlang expression
 * a cutlang evaluater as a root macro
 * a LHADA XML converter as a root macro

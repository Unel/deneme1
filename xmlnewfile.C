// Example to create a new xml file with the TXMLEngine class
// original Author: Sergey Linev
// Modified by NGU March 2016

#include "TXMLEngine.h"
#include "TString.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#define NS 100

using namespace std;

void xmlnewfile(const char* xfilename="deneme.xml", const char* ifilename="deneme.txt")
{
   TXMLEngine* xml = new TXMLEngine; // First create engine
   XMLNodePointer_t mainnode = xml->NewChild(0, 0, "main"); // Create main node of document tree
   ifstream ifile;
            ifile.open(ifilename);

   int k=0,j=0;
   int debug=2;
   string istr[NS]; 
   while (!ifile.eof() ){
     string aline; 
     while (getline(ifile, aline)){
         if (debug>1) std::cout << aline.size() <<" read.";
         for ( k=0; k<aline.size(); k++) {
             if (aline[k] == '#' ) { break;}
             if (debug>1) std::cout << aline[k];
             if (k>0 && aline[k] == ' ' && aline[k-1]!=' ') {cout <<"-space k="<<k<< " w="<<j<<endl; j=j+1;}
             elseif (k>1 && k == (aline.size()-1) && aline[k-1]!=' ') {cout <<"-EOL k="<<k<<" w="<<j<<endl; istr[j]+=aline[k]; j=j+1;}
             else istr[j]+=aline[k];
              
         }
        if (debug>1) std::cout <<" words="<< j << " k="<<k<<'\n';
        if (k<2) { //one object is read, lets XMLize it.
            if (debug>0) {
                    cout << "============\n";
                    for (int i=0; i<j; i++){ cout <<i<<" : "<<istr[i]<<"\n";}
                    std::cout<<endl;
            }
           // as a child node with attributes
           XMLNodePointer_t child = xml->NewChild(mainnode, 0, istr[0].data());
           xml->NewAttr(child, 0, "name",istr[1].data());
           for (int i=2; i<j; i=i+2){
            xml->NewAttr(child, 0, istr[i].data(),istr[i+1].data());
           }
           j=0; // reset the #words;
           for (int jj=0;jj<NS; jj++) istr[jj].clear(); //reset the string contents
        }
     }
  }
   



// now create doccumnt and assign main node of document
   XMLDocPointer_t xmldoc = xml->NewDoc();
   xml->DocSetRootElement(xmldoc, mainnode);
   xml->SaveDoc(xmldoc, xfilename); // Save document to file
   xml->FreeDoc(xmldoc);           // Release memory before exit
   delete xml;
}
